package com.example.biancavirtopeanu.oopquiz;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biancavirtopeanu.oopquiz.network.HttpConnection;
import com.example.biancavirtopeanu.oopquiz.network.HttpResponse;
import com.example.biancavirtopeanu.oopquiz.utils.Constants;
import com.example.biancavirtopeanu.oopquiz.utils.Question;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class QuizActivity extends AppCompatActivity {

    FloatingActionButton buttonCheck;
    FloatingActionButton buttonNext;
    CheckBox answer1;
    CheckBox answer2;
    CheckBox answer3;
    TextView questionText;
    private int index=0;
    private int score=0;

    private List<Question> questionList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        buttonCheck=(FloatingActionButton)findViewById(R.id.quiz_check);
        buttonNext=(FloatingActionButton)findViewById(R.id.quiz_next);
        answer1=(CheckBox)findViewById(R.id.quiz_c1);
        answer2=(CheckBox)findViewById(R.id.quiz_c2);
        answer3=(CheckBox)findViewById(R.id.quiz_c3);
        questionText=(TextView)findViewById(R.id.quiz_question);

        AlertDialog alertDialog = new AlertDialog.Builder(QuizActivity.this).create();
        alertDialog.setTitle("Info");
        alertDialog.setMessage(Constants.app_info);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();




        @SuppressLint("StaticFieldLeak") HttpConnection httpConnection = new HttpConnection(){
            @Override
            protected void onPostExecute(HttpResponse httpResponse) {
                super.onPostExecute(httpResponse);
                if(httpResponse!=null){
                    questionList=new ArrayList<>();
                    questionList.addAll(httpResponse.getQuestionList());
                    Collections.shuffle(questionList);

                    questionText.setText(questionList.get(index).getText());
                    answer1.setText(questionList.get(index).getAnswer1().getText());
                    answer2.setText(questionList.get(index).getAnswer2().getText());
                    answer3.setText(questionList.get(index).getAnswer3().getText());

                }
            }
        };httpConnection.execute(Constants.URL_FOR_JSON);

        setClickOnNext();
        setClickOnCheck();

    }

    private void setClickOnNext(){

            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (questionList != null) {
                        answer1.setChecked(false);
                        answer2.setChecked(false);
                        answer3.setChecked(false);
                        answer1.setBackgroundColor(0);
                        answer2.setBackgroundColor(0);
                        answer3.setBackgroundColor(0);
                        if (index < questionList.size()-1) {
                            index++;
                            questionText.setText(questionList.get(index).getText());
                            answer1.setText(questionList.get(index).getAnswer1().getText());
                            answer2.setText(questionList.get(index).getAnswer2().getText());
                            answer3.setText(questionList.get(index).getAnswer3().getText());
                        }else if(index==questionList.size()-1){
                            index=questionList.size()-1;
                            questionText.setText(questionList.get(index).getText());
                            answer1.setText(questionList.get(index).getAnswer1().getText());
                            answer2.setText(questionList.get(index).getAnswer2().getText());
                            answer3.setText(questionList.get(index).getAnswer3().getText());
                        }
                    }
                }
            });

    }

    private void setClickOnCheck(){

            buttonCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (questionList != null) {
                        if (index < questionList.size()-1) {
                            Question currentQuestion = questionList.get(index);

                            if (currentQuestion.getAnswer1().isValid()) {
                                answer1.setBackgroundColor(Color.GREEN);
                                if (answer1.isChecked()) {
                                    score++;
                                }
                            } else if (currentQuestion.getAnswer2().isValid()) {
                                answer2.setBackgroundColor(Color.GREEN);
                                if (answer2.isChecked()) {
                                    score++;
                                }
                            } else if (currentQuestion.getAnswer3().isValid()) {
                                answer3.setBackgroundColor(Color.GREEN);
                                if (answer3.isChecked()) {
                                    score++;
                                }
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "You have achieved " + score + " points", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            });


    }
}
