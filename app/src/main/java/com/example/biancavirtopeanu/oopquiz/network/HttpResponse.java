package com.example.biancavirtopeanu.oopquiz.network;

import com.example.biancavirtopeanu.oopquiz.utils.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bianca Virtopeanu on 1/29/2018.
 */

public class HttpResponse {

    private List<Question> questionList;

    public HttpResponse(List<Question> questionList) {
        this.questionList = questionList;
    }

    public HttpResponse() {
        this.questionList=new ArrayList<>();
    }

    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }
}
