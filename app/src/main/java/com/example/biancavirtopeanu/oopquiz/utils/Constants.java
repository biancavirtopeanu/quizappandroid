package com.example.biancavirtopeanu.oopquiz.utils;

/**
 * Created by Bianca Virtopeanu on 1/29/2018.
 */

public interface Constants {

    String URL_FOR_JSON="https://api.myjson.com/bins/pu2jp";
    String app_info="Hello and welcome! Something to be aware of: You cannot skip a question and return, so please do your best and try to answer." +
            "Afer you make your attempt, press the check button and the correct answer will be highlighted in green color. Also, the questions are from C++. Good luck!";

    //NODES FOR JSON
    String JSON_LIST="listOfQuestionsAndAnswers";
    String JSON_TEXT_ATTR="text";
    String JSON_VALID_ATTR="valid";
    String JSON_ANSWER1="answer1";
    String JSON_ANSWER2="answer2";
    String JSON_ANSWER3="answer3";
}
