package com.example.biancavirtopeanu.oopquiz.utils;

/**
 * Created by Bianca Virtopeanu on 1/29/2018.
 */

public class Answer {

    private String text;
    private boolean valid;

    public Answer() {
    }

    public Answer(String text, boolean valid) {
        this.text = text;
        this.valid = valid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "text='" + text + '\'' +
                ", valid=" + valid +
                '}';
    }
}
