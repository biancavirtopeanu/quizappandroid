package com.example.biancavirtopeanu.oopquiz.network;

import android.os.AsyncTask;

import com.example.biancavirtopeanu.oopquiz.utils.Answer;
import com.example.biancavirtopeanu.oopquiz.utils.Constants;
import com.example.biancavirtopeanu.oopquiz.utils.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bianca Virtopeanu on 1/29/2018.
 */

public class HttpConnection extends AsyncTask<String,Void,HttpResponse> {
    private URL url;
    private HttpURLConnection connection;
    @Override
    protected HttpResponse doInBackground(String... strings) {
        try {
            url=new URL(strings[0]);
            connection=(HttpURLConnection)url.openConnection();
            InputStream inputStream=connection.getInputStream();
            InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
            BufferedReader reader=new BufferedReader(inputStreamReader);

            String line;
            line=reader.readLine();
            return getInformationFromJson(line);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private HttpResponse getInformationFromJson(String json){
        if(json==null){
            return null;
        }else{
            try {
                List<Question> questionList=new ArrayList<>();
                JSONObject jsonObject=new JSONObject(json);
                JSONArray listOfQuestions=jsonObject.getJSONArray(Constants.JSON_LIST);
                for(int i=0;i<listOfQuestions.length();i++){
                    JSONObject question = listOfQuestions.getJSONObject(i);
                    Question q = new Question();
                    q.setText(question.getString(Constants.JSON_TEXT_ATTR));
                    JSONObject answer1=question.getJSONObject(Constants.JSON_ANSWER1);

                    Answer a1=new Answer();
                    a1.setText(answer1.getString(Constants.JSON_TEXT_ATTR));
                    a1.setValid(answer1.getBoolean(Constants.JSON_VALID_ATTR));
                    q.setAnswer1(a1);

                    JSONObject answer2=question.getJSONObject(Constants.JSON_ANSWER2);
                    Answer a2=new Answer();
                    a2.setText(answer2.getString(Constants.JSON_TEXT_ATTR));
                    a2.setValid(answer2.getBoolean(Constants.JSON_VALID_ATTR));
                    q.setAnswer2(a2);

                    JSONObject answer3=question.getJSONObject(Constants.JSON_ANSWER3);
                    Answer a3=new Answer();
                    a3.setText(answer3.getString(Constants.JSON_TEXT_ATTR));
                    a3.setValid(answer3.getBoolean(Constants.JSON_VALID_ATTR));
                    q.setAnswer3(a3);

                    questionList.add(q);
                }

                HttpResponse response=new HttpResponse();
                response.setQuestionList(questionList);
                return response;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
