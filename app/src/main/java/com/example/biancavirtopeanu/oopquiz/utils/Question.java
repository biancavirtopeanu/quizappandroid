package com.example.biancavirtopeanu.oopquiz.utils;

/**
 * Created by Bianca Virtopeanu on 1/29/2018.
 */

public class Question {

    private String text;
    private Answer answer1;
    private Answer answer2;
    private Answer answer3;


    private void setAnswerByIndex(Answer a, int i){
        if(i==0){
            this.answer1=a;
            return;
        }
        if(i==1){
            this.answer2=a;
            return;
        }
        if(i==2){
            this.answer3=a;
            return;
        }
    }

    public Question(String text, Answer answer1, Answer answer2, Answer answer3) {
        this.text = text;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
    }

    public Question() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Answer getAnswer1() {
        return answer1;
    }

    public void setAnswer1(Answer answer1) {
        this.answer1 = answer1;
    }

    public Answer getAnswer2() {
        return answer2;
    }

    public void setAnswer2(Answer answer2) {
        this.answer2 = answer2;
    }

    public Answer getAnswer3() {
        return answer3;
    }

    public void setAnswer3(Answer answer3) {
        this.answer3 = answer3;
    }
}
